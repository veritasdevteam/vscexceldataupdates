﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSCExcelDataUpdates
{
    class Program
    {
        static void Main(string[] args)
        {
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Open(@"C:\Users\ChadLutz\source\repos\ExcelFiles\[FILE_NAME].xlsx");
            Microsoft.Office.Interop.Excel.Worksheet ws = wb.Worksheets[1];
            Microsoft.Office.Interop.Excel.Range rng = ws.UsedRange;
            int RowCount = rng.Rows.Count;
            int ColCount = rng.Columns.Count;
            clsDBO.clsDBO dBO = new clsDBO.clsDBO();
            string sSQL, ContractNo = "", Program = "", Plan = "", VClass = "";
            string ConnectionString = "server=198.143.98.122;database=veritas;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
            for (int row = 2; row <= RowCount; row++)
            {
                for (int col = 1; col <= ColCount; col++)
                {
                    Microsoft.Office.Interop.Excel.Range range = ws.Cells[row, col];
                    string CellValue = range.Value.ToString();
                    if (col == 4) 
                    { 
                    }
                    else
                    {
                        if (col == 1) ContractNo = CellValue.Trim();
                        else if (col == 2)
                        {
                            string substring = "AN ";
                            int indexOfSubstring = CellValue.IndexOf(substring);
                            Program = CellValue.Remove(indexOfSubstring, substring.Length).Trim();
                        }
                        else if (col == 3) Plan = CellValue.Trim();
                        else if (col == 5) VClass = CellValue.Trim();
                    }
                    range.Interior.Color = Microsoft.Office.Interop.Excel.XlRgbColor.rgbYellow;
                }
                sSQL = "update VeritasMoxy.dbo.MoxyContract " +
                       "set program = '" + Program + "', " +
                       "VSCPlan = '" + Plan + "', " +
                       "class = '" + VClass + "' " +
                       "where contractno = '" + ContractNo + "' ";

                dBO.RunSQL(sSQL, ConnectionString);
                Console.WriteLine(row+" "+ContractNo+" "+Program+" "+Plan+" "+VClass);
            }
            wb.Save();
            wb.Close();
            app.Quit();
            Console.WriteLine("\n\nPress Enter to close the program");
            Console.ReadLine();
        }
    }
}
